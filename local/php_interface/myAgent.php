<?php

use \Bitrix\Main\Web\HttpClient;

function OrgAgentPeriod()
{
	$httpClient = new HttpClient();
	$arHeaders = [
		'accept'            => 'application/json; charset=utf-8',
		'accept-language'   => 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
		'cache-control'     => 'no-cache',
		'if-modified-since' => 0,
		'pragma'            => 'no-cache',
		'request-guid'      => '72a3f5d1-e67d-4671-bdcc-f8d0f384afb5',
		'sec-fetch-dest'    => 'empty',
		'sec-fetch-mode'    => 'cors',
		'sec-fetch-site'    => 'same-origin',
		'session-guid'      => 'f1bf0dd1-287a-44a8-bada-bc38278fb9bb',
		'state-guid'        => '/org-info',
	];
	foreach ($arHeaders as $strTitle => $strValue) {
		$httpClient->setHeader($strTitle, $strValue, true);
	}
	$arOrganization = json_decode($httpClient->get('https://dom.gosuslugi.ru/information-disclosure/api/rest/services/disclosures/org/common/9e96a547-3f89-449e-a3cc-4f8aa873f40e'), true);
	if (!empty(CIBlockElement::GetList(
				[],
				[
					'IBLOCK_CODE'  => 'organization',
					'PROPERTY_INN' => $arOrganization['detailInfo']['inn']
				],
				false,
				false,
				[
					'ID'
				]
			)->Fetch()['ID']
		)
	) {
		$arCril = current($arOrganization['citizenReceptionInfoList']);
		$arCrhOh = current($arOrganization['citizensReceptionHours'])['openHours'];
		$arLicense = current($arOrganization['licenses']);
		$arProps = [
			'INN'                               => $arOrganization['detailInfo']['inn'],
			'MANAGER'                           => $arCril['employeePosition'],
			'ADDRESS'                           => $arOrganization['detailInfo']['fullName'],
			'ADDRESS_ZIP'                       => $arCril['house']['postalCode'],
			'ADDRESS_CITY'                      => $arCril['region']['formalName'],
			'ADDRESS_STREET'                    => $arCril['street']['formalName'],
			'ADDRESS_HOUSE'                     => $arCril['house']['houseNumber'],
			'PHONE'                             => current($arOrganization['detailInfo']['phones']),
			'INFO_NAME_FULL'                    => $arOrganization['detailInfo']['fullName'],
			'INFO_NAME_SHORT'                   => $arOrganization['detailInfo']['name'],
			'INFO_OKOPF'                        => $arOrganization['detailInfo']['okopf'],
			'INFO_FIO'                          => $arCril['employeePosition'],
			'INFO_INN'                          => $arOrganization['detailInfo']['inn'],
			'INFO_OGRN'                         => $arOrganization['detailInfo']['ogrn'],
			'INFO_LEGAL_ADDRESS'                => $arOrganization['detailInfo']['regAddress'],
			'INFO_ACTUAL_ADDRESS'               => $arOrganization['detailInfo']['managingAuthorityAddress'],
			'INFO_POST_ADDRESS'                 => $arOrganization['detailInfo']['postAddress'],
			'INFO_WORK_TIME'                    => explode(' ', $arCrhOh['beginDate'])[1] . ' — ' . explode(' ', $arCrhOh['endDate'])[1],
			'INFO_DISPATCHER_ADDRESS'           => $arOrganization['detailInfo']['managingAuthorityAddress'],
			'INFO_DISPATCHER_PHONE'             => implode('; ', $arOrganization['dispatchingServicePhones']),
			'INFO_PHONE'                        => current($arOrganization['detailInfo']['phones']),
			'INFO_FAX'                          => current($arOrganization['detailInfo']['faxes']),
			'INFO_EMAIL'                        => $arOrganization['detailInfo']['email'],
			'INFO_SITE'                         => $arOrganization['detailInfo']['www'],
			'INFO_UNDER_MANAG_HOUSES_COUNT'     => $arOrganization['detailInfo']['managedHouseCount'],
			'INFO_UNDER_MANAG_HOUSES_SQUARE'    => $arOrganization['detailInfo']['managedHouseSquare'],
			'INFO_STAFF_REGULAR_TOTAL'          => $arOrganization['staffInfo']['totalStaffCount'],
			'INFO_STAFF_REGULAR_ADMINISTRATIVE' => $arOrganization['staffInfo']['administrativeStaffCount'],
			'INFO_STAFF_REGULAR_ENGINEERS'      => $arOrganization['staffInfo']['engineeringStaffCount'],
			'INFO_STAFF_REGULAR_LABOR'          => $arOrganization['staffInfo']['workingStaffCount'],
			'INFO_MEMBERSHIP_INFORMATION'       => current($arOrganization['detailInfo']['participationInAuthorizedCapitalInfoList'])['authorityName'],
			'INFO_LICENSE_NUMBER'               => $arLicense['licenseNumber'],
			'INFO_LICENSE_RECEIPT_DATE'         => $arLicense['licenseDate'],
			'INFO_LICENSE_ISSUING_AUTHORITY'    => $arLicense['licensingAuthority'],
			'INFO_LICENSE_FILE'                 => '' // ссылок в массиве нет
		];
		$arProps['WORK_TIME'] = '<dl>';
		$arDaysOfWeek = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
		foreach ($arOrganization['schedule']['organizationOpeningHours'] as $key => $arDay) {
			$arProps['WORK_TIME'] .= '<dt><p>' . $arDaysOfWeek[$key] . ':</p></dt><dd><p>' . (empty($arDay['openHours']['beginDate']) ? 'Выходной' : '<p><strong>Часы работы<strong>: ' . explode(' ', $arDay['openHours']['beginDate'])[1] . ' — ' . explode(' ', $arDay['openHours']['endDate'])[1] . '</p><p><strong>Перерыв</strong>: ' . explode(' ', $arDay['breakHours']['beginDate'])[1]) . ' — ' . explode(' ', $arDay['openHours']['endDate'])[1] . '</p></dd>';
		}
		$arProps['WORK_TIME'] .= '</dl>';
		$arProps['INFO_DISPATCHER_WORK_TIME'] = $arProps['WORK_TIME'];
		$arLoadProductArray = [
			'ACTIVE'            => 'Y',
			'DETAIL_TEXT'       => $arOrganization['detailInfo']['okopf'],
			'MODIFIED_BY'       => $USER->GetID(),
			'IBLOCK_CODE'       => 18,
			'IBLOCK_SECTION_ID' => false,
			'NAME'              => $arOrganization['detailInfo']['name'],
			'PREVIEW_TEXT'      => $arOrganization['detailInfo']['fullName'],
			'PROPERTY_VALUES'   => $arProps
		];
	}
	return 'OrgAgentPeriod();';
}