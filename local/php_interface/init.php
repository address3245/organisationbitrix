<?php

if (!empty($_GET['noinit'])) {
	if (($strNoInit = strval($_GET['noinit'])) === 'N') {
		if (!empty($_SESSION['NO_INIT'])) {
			unset($_SESSION['NO_INIT']);
		}
	} else if ($strNoInit === 'Y') {
		$_SESSION['NO_INIT'] = 'Y';
	}
}
if ($_SESSION['NO_INIT'] !== 'Y' && file_exists($strAgentPath = $_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/myAgent.php')) {
	require_once($strAgentPath);
}